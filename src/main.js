import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import axios from 'axios'


window.token = localStorage.getItem('token')

window.axios = axios

window.axios.defaults.baseURL = 'http://localhost:8000';

window.axios.defaults.params={api_token:window.token}

// window.axios.defaults.headers.common['api_token'] =window.token;
// window.axios.defaults.params = {};
// window.axios.defaults.params[ 'api_token' ] = window.token;

Vue.config.productionTip = false

// window.event = new Vue;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
